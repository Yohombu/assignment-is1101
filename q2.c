#include<stdio.h>
//This programme will compute the area of disk

int main()
{
    float r,area;
    printf("Enter the radius of disk\n"); // radius is r
    scanf("%f",&r);
    area=3.14*r*r;
    printf("Area of disk = %.3f\n",area);
    return 0;
}
