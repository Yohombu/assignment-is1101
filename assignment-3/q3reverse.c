#include <stdio.h>

int main(){
 int num=0,reverse=0;
	
 printf("Enter an integer : ");
 scanf("%d",&num);
	
	//find the reverse number of num
	
 while (num != 0)
 {
   reverse*=10;
   reverse+=num%10;
   num/=10;
 }
  	
 printf("Reversed Number : %d\n",reverse);

 return 0;
}
