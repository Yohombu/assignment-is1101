#include<stdio.h>
//This programme will swap two integers.
int main() 
{
    int a,b,temp;
    printf("Enter 1st integer a : \n");
    scanf("%d",&a);
    printf("Enter 2nd integer b : \n");
    scanf("%d",&b);
    printf("Before Swapping, a = %d , b = %d\n",a,b);
    temp=a;
    a=b;
    b=temp;
    printf("After swapping, a = %d , b = %d\n",a,b);
    return 0;
}
